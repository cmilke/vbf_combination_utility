import sys
import math
import uproot #uproot4 (FOUR!!!)
import numpy
import matplotlib
from matplotlib import pyplot as plt

import combination_utils



def plot_histogram(hist_name, hist_title, edge_list, coupling_parameters,
        linearly_combined_weights, linearly_combined_errors,
        verification_weights, verification_errors,
        alt_linearly_combined_weights=None, alt_linearly_combined_errors=None,
        alt_label='1D Combination',
        range_specs=None, xlabel='$m_{HH}$ (GeV)',
        generated_label='Linear Combination', generated_color='blue'):

    print('Plotting '+hist_name+' for ' + str(coupling_parameters))
    draw_verify = type(verification_weights) != type(None) and type(verification_errors) != type(None)
    draw_alt_comb = type(alt_linearly_combined_weights) != type(None) and type(alt_linearly_combined_errors) != type(None)

    if draw_verify:
        fig, (ax_main, ax_ratio) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios':[4,1]} )
    else:
        fig, ax_main = plt.subplots()


    xpositions = 0.5*(edge_list[1:]+edge_list[:-1])
    counts, bins, points = ax_main.errorbar( xpositions, linearly_combined_weights,
        yerr=linearly_combined_errors, label=generated_label,
        marker='+', markersize=2, capsize=2, color=generated_color, linestyle='none', linewidth=1, zorder=3)

    if draw_alt_comb:
        alt_counts, alt_bins, alt_points = ax_main.errorbar( xpositions, alt_linearly_combined_weights,
            yerr=alt_linearly_combined_errors, label=alt_label,
            marker='x', markersize=2, capsize=2, color='purple', linestyle='none', linewidth=1, zorder=3)

    if draw_verify:
        vcounts, vbins, vhists = ax_main.hist( [edge_list[:-1]]*2,
            weights=[verification_weights-verification_errors, 2*verification_errors],
            label=['Generated MC', 'MC Statistical Error'],
            bins=edge_list, fill=True, histtype='barstacked', zorder=1, alpha=0.5, color=['green','red'])
        plt.setp(vhists[1], hatch='/////')

        safe_error = verification_errors.copy()
        safe_error[ safe_error == 0 ] = float('inf')
        safe_combination = linearly_combined_weights.copy()
        safe_combination[ safe_combination == 0 ] = float('inf')
        rcounts, rbins, rpoints = ax_ratio.errorbar( xpositions, (linearly_combined_weights-verification_weights)/safe_error,
            yerr=linearly_combined_errors/safe_error, label='MC Statistical Error check',
            marker='+', markersize=2, capsize=2, color=generated_color, linestyle='none', linewidth=1, zorder=3)

        if draw_alt_comb:
            alt_rcounts, alt_rbins, alt_rpoints = ax_ratio.errorbar( xpositions, (alt_linearly_combined_weights-verification_weights)/safe_error,
                yerr=alt_linearly_combined_errors/safe_error, label='MC Statistical Error check',
                marker='x', markersize=2, capsize=2, color='purple', linestyle='none', linewidth=1, zorder=3)

        ax_ratio.set(ylabel=r'$\frac{lin. comb. - gen.}{stat. error}$', xlabel=xlabel)
        ax_ratio.set_ylim([-3,3])
        ax_ratio.set_yticks(ticks=[-2,-1,0,1,2])
        zero_line = ax_ratio.hlines(0,xmin=edge_list[0],xmax=edge_list[-1],colors='black',zorder=2)
        ax_ratio.grid()

    kappa_labels = [ f'{param:.1f}' for param in coupling_parameters ]
    title  = hist_title+'\nfor '
    title += '$\kappa_{2V}='+kappa_labels[0]+'$, '
    title += '$\kappa_{\lambda}='+kappa_labels[1]+'$, '
    title += '$\kappa_{V}='+kappa_labels[2]+'$'
    fig.suptitle(title)

    ax_main.set(ylabel='Bin Weight')
    ax_main.legend(prop={'size':7})
    ax_main.grid()

    dpi=500
    kappa_string_list = [ label.replace('.','p') for label in kappa_labels ]
    kappa_string = 'cvv'+kappa_string_list[0]+'cl'+kappa_string_list[1]+'cv'+kappa_string_list[2]
    fig.tight_layout()
    figname = hist_name+'_'+kappa_string
    fig.savefig(figname+'.pdf')
    plt.close()


def read_datafile(datafile, kinematic_variable_bin_edges):
    # Read in datafile
    tree_name = 'sig'
    rootfile = uproot.open(datafile)
    ttree = rootfile[tree_name]

    # Apply final signal selections on events
    pass_vbf_sel = ttree['pass_vbf_sel'].array()
    x_wt_tag = ttree['X_wt_tag'].array() > 1.5
    ntag = ttree['ntag'].array() >= 4
    valid_event = numpy.logical_and.reduce( (pass_vbf_sel, x_wt_tag, ntag) )

    # Retrieve kinematic variable (mhh) and weights for all selected events
    kinematic_variable_array = numpy.array( ttree['m_hh'].array()[valid_event] )
    event_weight_array = numpy.array( ttree['mc_sf'].array()[:,0][valid_event] )

    # Bin events into a histogram
    histogram_bin_weights = numpy.histogram(kinematic_variable_array,
            bins=kinematic_variable_bin_edges,
            weights=event_weight_array)[0] # I don't need [1], which is just the bin edges

    # Calculate root-style statistical error for each bin
    # ... one of the few things that numpy *can't* do more easily than root
    histogram_bin_errors = numpy.zeros( len(histogram_bin_weights) )
    event_bins = numpy.digitize(kinematic_variable_array,kinematic_variable_bin_edges)-1
    for i in range(len(histogram_bin_errors)):
        binned_weights = event_weight_array[ event_bins == i ]
        error_squared_array = binned_weights**2
        stat_error = math.sqrt( error_squared_array.sum() )
        histogram_bin_errors[i] = stat_error

    return (histogram_bin_weights, histogram_bin_errors)



def main():
    datafile_list = [
        #(k2v  ,  kl , kv   ),    Datafile
        ( (1    ,  1  , 1   ),  'input/vbf_502970_mc16e.root'),
        ( (0    ,  1  , 1   ),  'input/vbf_502971_mc16e.root'),
        ( (0.5  ,  1  , 1   ),  'input/vbf_502972_mc16e.root'),
        ( (1.5  ,  1  , 1   ),  'input/vbf_502973_mc16e.root'),
        ( (2    ,  1  , 1   ),  'input/vbf_502974_mc16e.root'),
        ( (3    ,  1  , 1   ),  'input/vbf_502975_mc16e.root'),
        ( (1    ,  0  , 1   ),  'input/vbf_502976_mc16e.root'),
        ( (1    ,  2  , 1   ),  'input/vbf_502977_mc16e.root'),
        ( (1    ,  10 , 1   ),  'input/vbf_502978_mc16e.root'),
        ( (1    ,  1  , 0.5 ),  'input/vbf_502979_mc16e.root'),
        ( (1    ,  1  , 1.5 ),  'input/vbf_502980_mc16e.root'),
        ( (0    ,  0  , 1   ),  'input/vbf_502981_mc16e.root')
    ]

    coupling_basis_list = [
        (1.0, 1.0, 1.0),
        (0.5, 1.0, 1.0),
        (3.0, 1.0, 1.0),
        (1.0, 2.0, 1.0),
        (1.0, 10.0, 1.0),
        (0.0, 0.0, 1.0)
    ]

    kinematic_variable_bin_edges = numpy.linspace(200, 2000, 55) #mhh range in GeV

    # Read in all data files as numpy arrays
    input_data_dictionary = { couplings: read_datafile(datafile, kinematic_variable_bin_edges) for couplings, datafile in datafile_list }

    # Construct combination basis
    # The combination constructor expects input in the form 
    # of a dictionary of the basis couplings and their corresponding histograms, e.g.:
    # basis_inputs = { (k2v_1, kl_1, kv_1) : (histogram_1_bin_weights, histogram_1_bin_errors),
    #                  (k2v_2, kl_2, kv_2) : (histogram_2_bin_weights, histogram_2_bin_errors), ... }
    basis_inputs = { couplings:input_data_dictionary[couplings] for couplings in coupling_basis_list }
    combination_function = combination_utils.Linear_combination_construct(basis_inputs)

    # Validate combination
    for couplings, input_data in input_data_dictionary.items():
        verification_weights, verification_errors = input_data
        combined_weights, combined_errors = combination_function(couplings)

        plot_histogram('reco_mHH', 'NNT-Based Linear Combination:\n$m_{HH}$',
                kinematic_variable_bin_edges, couplings,
                combined_weights, combined_errors,
                verification_weights, verification_errors,
                xlabel='Reconstructed $m_{HH}$ (GeV)'
        )


if __name__ == '__main__': main()
