import sympy
import numpy
import re

# Define symbols used for couplings
_k2v = sympy.Symbol('\kappa_{2V}')
_kl = sympy.Symbol('\kappa_{\lambda}')
_kv = sympy.Symbol('\kappa_{V}')

# For further explanation of the math used in this script,
# please refer to slides 3-5 of this presentation:
# https://indico.cern.ch/event/1045646/contributions/4392452/attachments/2257944/3831732/dihiggs_4b_210603.pdf
# Slide 5 in particular will be reffered to repeatedly


# These define what kind of "scan" you want to do
# (e.g. k2v only, klambda, or k2v/klambda/kv 3D)
# The functions here correspond to the functional
# dependence of the various terms in the matrix-element
# expansion of VBF cross-section
# (they directly correspond to the "f" vector on slide 5)
_kl_scan_terms = [
    lambda k2v,kl,kv: kl**2,
    lambda k2v,kl,kv: kl,
    lambda k2v,kl,kv: 1
]

_k2v_scan_terms = [
    lambda k2v,kl,kv: k2v**2,
    lambda k2v,kl,kv: k2v,
    lambda k2v,kl,kv: 1
]

_kl_k2v_scan_terms = [
    lambda k2v,kl,kv: kl**2,
    lambda k2v,kl,kv: k2v**2,
    lambda k2v,kl,kv: kl,
    lambda k2v,kl,kv: k2v * kl,
    lambda k2v,kl,kv: k2v,
    lambda k2v,kl,kv: 1
]

_full_scan_terms = [
    lambda k2v,kl,kv: kv**2 * kl**2,
    lambda k2v,kl,kv: kv**4,
    lambda k2v,kl,kv: k2v**2,
    lambda k2v,kl,kv: kv**3 * kl,
    lambda k2v,kl,kv: k2v * kl * kv,
    lambda k2v,kl,kv: kv**2 * k2v
]

# Aggregate all the above lists for easy reference
_equation_dictionary = {
    'fullscan': _full_scan_terms,
    'kl_k2v': _kl_k2v_scan_terms,
    'k2v': _k2v_scan_terms,
    'kl': _kl_scan_terms
}


# Use sympy to ouput a compilable latex file
def generate_latex(amplitudes, basis_states, final_amplitude, name=None):
    substitutions = [ (a, r'sigma'f'({i},{j},{k})') for a, (i,j,k) in zip(amplitudes, basis_states) ]
    out_equation = final_amplitude.subs(substitutions)
    with open('final_amplitude_'+name+'.tex','w') as texfile:
        formatted_equation = sympy.latex(out_equation)
        formatted_equation = formatted_equation.replace(r'\sigma', r'\times \sigma')
        formatted_equation = re.sub(r'(\\sigma\{\\left\([^,]*,[^,]*,[^\\]*\\right\)\} \+)', r'\1$\n\n$', formatted_equation)
        formatted_equation = '$'+formatted_equation+'$\n'
        tex_output  = r'\documentclass{beamer}''\n'r'\begin{document}''\n'r'\frame{''\n'
        tex_output += formatted_equation
        tex_output += '}\n'r'\end{document}'
        print('\n'+formatted_equation)
        texfile.write(tex_output)
        print('\n-------------------------------')
        print('Created tex file: '+texfile.name)
        print('Compile using: make '+texfile.name[:-4])
        print('-------------------------------\n')


# Performs linear algebra to determine the appropriate equation needed 
#   to combine a set of samples together.
#
# Returns a tuple of (bool, lambda):
#   The bool indicates whether the given basis_parameters are linearly independent;
#   The lambda function produces the appropriate coefficients for each sample,
#   for some given coupling values
#
# If only_check is set to True, the function will only check if the parameters are linearly independent,
#   without generating a coefficient function
#
# If output is set to 'ascii' or 'tex', the function will produce
#   a human-readable form of the combination function
def get_coefficient_function( basis_parameters, equation_set='fullscan',
        name='unnamed', output=None, only_check=False):

    if type(equation_set) == list:
        base_equations = equation_set
    elif type(equation_set) == str:
        base_equations = _equation_dictionary[equation_set]
    else:
        print('Cannot interpret equation_set of type '+str(type(base_equations)))
        print(equation_set)
        exit(1)

    # Convert the coupling values provided into something usable by sympy
    basis_states = [ [ sympy.Rational(param) for param in basis ] for basis in basis_parameters ]

    # Construct the set of linear equations to be solved, in the form of a matrix
    # (this corresponds to the "F" matrix on slide 5)
    combination_matrix = sympy.Matrix([ [ g(*base) for g in base_equations ] for base in basis_states])

    # Make sure the matrix determinate is non-singular
    # If this fails, the chosen basis is not linearly independent,
    # and you will need another basis
    if combination_matrix.det() == 0: return (False, None)
    if only_check: return (True, None)

    # Invert the matrix to solve the set of equations
    # (This is the second line of slide 5)
    inversion = combination_matrix.inv()

    # Create the generic "f(κ2V,κλ,κV)" vector at the bottom of slide 5,
    # which will be used to plug in arbitrary coupling values
    term_vector = sympy.Matrix([ [g(_k2v,_kl,_kv)] for g in base_equations ])

    # If output is requested, print either to stdout or generate a small tex file
    if type(output) != type(None):
        amplitudes = sympy.Matrix([ sympy.Symbol(f'A{n}') for n in range(len(base_equations)) ])
        # Numpy outputs a 1x1 matrix here, so I use the [0] to get just the equation
        final_amplitude = (term_vector.T*inversion*amplitudes)[0]
        if output == 'ascii': sympy.pprint(final_amplitude)
        if output == 'tex': generate_latex(amplitudes, basis_states, final_amplitude, name=name)

    # Produce the final coefficient vector function,
    # which is rearranged such that each coefficient will act as a 
    # coefficient to an individual sample/cross-section
    final_weight = term_vector.T * inversion
    coefficient_vector_function = sympy.lambdify([_k2v, _kl, _kv], final_weight, 'numpy')
    return (True, coefficient_vector_function)



class Linear_combination_construct:
    # This takes a dictionary of couplings and their associated histograms
    def __init__(self, basis_inputs, equation_set='fullscan'):
        basis_coupling_list, basis_data_list = list(zip(*basis_inputs.items()))
        linearly_independent, coefficient_function = get_coefficient_function(basis_coupling_list, equation_set=equation_set)
        if not linearly_independent:
            print('Supplied with set of couplings which are not linearly independent:')
            print(basis_coupling_list)
            print('\nExiting')
            exit(1)

        weight_list, error_list = list(zip(*basis_data_list))
        self.basis_weights = numpy.array(weight_list)
        self.basis_errors = numpy.array(error_list)
        self.coefficient_vector_function = coefficient_function

    def __call__(self, coupling_parameters):
        multiplier_vector = self.coefficient_vector_function(*coupling_parameters)[0]

        reweighted_histograms = multiplier_vector[:,None] * self.basis_weights
        linearly_combined_weights = reweighted_histograms.sum(axis=0)

        reweighted_errors2 = (multiplier_vector[:,None] * self.basis_errors)**2
        linearly_combined_errors = numpy.sqrt( reweighted_errors2.sum(axis=0) )

        return linearly_combined_weights, linearly_combined_errors



def main():
    recommended3D_basis_202106 = [
    #   (k2v, kl , kv )
        (1.0, 1.0, 1.0),
        (0.5, 1.0, 1.0),
        (3.0, 1.0, 1.0),
        (1.0, 2.0, 1.0),
        (1.0, 10.0, 1.0),
        (0.0, 0.0, 1.0)
    ]

    get_coefficient_function( recommended3D_basis_202106, output='tex', name='example' )


if __name__ == '__main__': main()
