# NOTE: this makefile is only used for converting the tex file
# (produced by combination_utils.py) into a readable pdf

outdir = pdfout
compile = lualatex -halt-on-error -output-directory $(outdir) -pdf

%: %.tex $(outdir)
	$(compile) $<
	@echo -e "\nGenerated "$@.pdf" in directory '$(outdir)'"

$(outdir):
	mkdir $(outdir)

clean:
	rm $(outdir)/* &> /dev/null
